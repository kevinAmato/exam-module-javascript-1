const srcListe = [
	{
        id: 0,
		name: "Anthony Drapeau",
		country: "Espagne"
	},
	{
        id: 1,
		name: "Mickael Colere",
		country: "Danemark"
	},
	{
        id: 2,
		name: "Lucien Marcheciel",
		country: "France"
	}
]

module.exports = srcListe;
