export class User {
    _id;
    _name;
    _country;

    constructor(id, name, country) {
        id !== null ? (this.id = id) : (this.id = this._randomId());
        this.name = name;
        this.country = country;
    }

    get name() {
        return this._name;
    }

    set name(tmp) {
        this._name = tmp;
    }

    get country() {
        return this._country;
    }

    set country(tmp) {
        this._country = tmp;
    }

    get id() {
        return this._id;
    }

    set id(tmp) {
        this._id = tmp;
    }

    initUser(liste) {
        let containerListe = document.querySelector("#classUser");

        let myUser = document.createElement("div");
        let myTitle = document.createElement("h2");
        let myP = document.createElement("p");
        myP.innerHTML = `<strong>Pays :</strong> ${this.country}`;
        let myLink = document.createElement("a");

        let del = document.createElement("a");
        let edit = document.createElement("a");

        if (liste) {
            myLink.href = "./user.html#" + this.id;
            myLink.innerText = "détails";
        } else {
            edit.className = "edit";
            edit.href = "./edit.html#" + this.id;
            edit.innerText = " Edit \n";

            del.className = "del";
            del.href = "./liste.html";
            del.innerText = "Supprimer";
        }

        if (this.id % 2) {
            myUser.style.backgroundColor = "lightblue";
        } else {
            myUser.style.backgroundColor = "lightgrey";
        }

        myTitle.innerText = this.name;

        containerListe.appendChild(myUser);
        myUser.appendChild(myTitle);
        myUser.appendChild(myP);

        if (liste) {
            myUser.appendChild(myLink);
        } else {
            myUser.appendChild(edit);
            myUser.appendChild(del);
        }
    }

    //permet de renvoyer l'objet au bon format json sans avoir à supprimer les '_' des propriétés privés
    toJson() {
        let json = {
            id: this._id,
            name: this._name,
            country: this._country,
        };
        return json;
    }

    _randomId() {
        return Date.now();
    }
}
