import { User } from "./class.user.js";

let myId = +window.location.hash.split("#")[1];

let myHeaders = new Headers();
let url = "/liste/" + myId;
let options = {
    method: "GET",
    headers: myHeaders,
    mode: "cors",
    cache: "default",
};

fetch(url, options)
    .then((res) => {
        if (res.ok) {
            return res.json();
        }
    })
    .then((response) => {
        let valeurs = document.querySelectorAll("input");
        valeurs[0].value = response.name;
        valeurs[1].value = response.country;

        // récupère les données du formulaire et modifie un utilisateur dans la "base".
        document.querySelector("#editUser").addEventListener("submit", (e) => {
            e.preventDefault();

            valeurs[0].value = valeurs[0].value ? valeurs[0].value : "X";
            valeurs[1].value = valeurs[1].value ? valeurs[1].value : "France";

            let user = new User(myId, valeurs[0].value, valeurs[1].value);
            user = user.toJson();

            //Modification des options
            let options = {
                method: "PUT",
                headers: {
                    Accept: "application/json",
                    "Content-Type": "application/json",
                },
                mode: "cors",
                cache: "default",
                body: JSON.stringify(user),
            };

            fetch(url, options)
                .then((res) => {
                    if (res.ok) {
                        return res.json();
                    }
                })
                .then((response) => {
                    alert(`Utilisateur Modifié avec succès!`);
                    window.location = "../pages/liste.html";
                });
        });
    });
