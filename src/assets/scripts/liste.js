import { User } from "./class.user.js";

let myHeaders = new Headers();
let url = "/liste";
let options = {
    method: "GET",
    headers: myHeaders,
    mode: "cors",
    cache: "default",
};

// retourne la liste des elements
fetch(url, options)
    .then((res) => {
        if (res.ok) {
            return res.json();
        }
    })
    .then((response) => {
        response.forEach((elt) => {
            let user = new User(elt.id, elt.name, elt.country);
            user.initUser(true);
        });
        // récupère les données du formulaire et ajoute un nouvel utilisateur dans la "base".
        document.querySelector("#postUser").addEventListener("submit", (e) => {
            e.preventDefault();

            const valeurs = document.querySelectorAll("input");

            valeurs[0].value = valeurs[0].value
                ? valeurs[0].value
                : "Monsieur Dupont";
            valeurs[1].value = valeurs[1].value ? valeurs[1].value : "France";

            let user = new User(null, valeurs[0].value, valeurs[1].value);

            user.initUser(true);
            user = user.toJson();

            //définition de la méthode POST
            let options = {
                method: "POST",
                headers: {
                    Accept: "application/json",
                    "Content-Type": "application/json",
                },
                mode: "cors",
                cache: "default",
                body: JSON.stringify(user),
            };

            fetch(url, options)
                .then((res) => {
                    if (res.ok) {
                        return res.json();
                    }
                })
                .then((response) => {
                    console.log(response);
                    alert(`Utilisateur ajouté avec succès!`);
                });
        });
    });
