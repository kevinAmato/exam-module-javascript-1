import { User } from "./class.user.js";
let myId = window.location.hash.split("#")[1];

let myHeaders = new Headers();
let url = "/liste/" + myId;
let options = {
    method: "GET",
    headers: myHeaders,
    mode: "cors",
    cache: "default",
};

fetch(url, options)
    .then((res) => {
        if (res.ok) {
            return res.json();
        }
    })
    .then((response) => {
        let user = new User(response.id, response.name, response.country);
        user.initUser(false);

        // Supprime l'utilisateur courant
        document.querySelector(".del").addEventListener("click", (e) => {
            e.preventDefault();
            options.method = "DELETE";

            fetch(url, options)
                .then((res) => {
                    if (res.ok) {
                        return res.json();
                    }
                })
                .then((response) => {
                    alert("Utilisateur supprimé avec succès!");
                    window.location = "../pages/liste.html";
                });
        });
    });
