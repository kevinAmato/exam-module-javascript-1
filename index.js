const express = require("express");
const app = express();

// import du jeu de donnée
let Liste = require("./data/Liste");

//lancement du serveur sur le port 3000
app.listen(3000, () => {
    console.log("Server is ON : localhost:3000");
});

//middleware
app.use(express.json());

app.use("/pages", require("express").static("./src/pages"));
app.use("/assets", require("express").static("./src/assets"));

app.get("/", (req, res) => {
    res.sendFile(__dirname + "/src/index.html");
});

app.get("/liste", (req, res) => {
    res.send(Liste);
});

app.get("/liste/:id", (req, res) => {
    const id = +req.params.id;
    const resquetedUser = Liste.filter((x) => x.id === id);
    res.send(resquetedUser[0]);
});

app.post("/liste", (req, res) => {
    Liste.push(req.body);
    res.send(Liste);
});

app.put("/liste/:id", (req, res) => {
    const id = +req.params.id;
    console.log(req.body);

    Liste.forEach((element) => {
        if (element.id == id) {
            element.name = req.body.name;
            element.country = req.body.country;
        }
    });
    res.send(Liste);
});

app.delete("/liste/:id", (req, res) => {
    const id = +req.params.id;

    Liste.forEach((element) => {
        element.id === id ? (Liste = Liste.filter((x) => x.id !== id)) : null;
    });
    res.send(Liste);
});
